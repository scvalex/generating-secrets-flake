pkgs:
with builtins; with pkgs.lib;
{
  fsn-qws-kube1 = {
    privateIpAddress = "10.10.0.10";
    kubernetes.enable = true;
  };

  fsn-qws-kube2 = {
    privateIpAddress = "10.10.0.11";
    kubernetes.enable = true;
  };

  fsn-qws-kube3 = {
    privateIpAddress = "10.10.0.12";
    etcd.enable = true;
    kubernetes.enable = true;
  };

  fsn-qws-etcd1 = {
    privateIpAddress = "10.10.0.13";
    etcd.enable = true;
  };

  fsn-qws-etcd2 = {
    privateIpAddress = "10.10.0.14";
    etcd.enable = true;
  };
}
