{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = github:NixOS/nixpkgs;
  };
  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          machines = import ./machines.nix pkgs;
        in
        {
          apps.genCerts = {
            type = "app";
            program = import ./gen-certs.nix pkgs machines;
          };
          apps.showCert = {
            type = "app";
            program = toString (pkgs.writers.writeBash "show-cert" ''
              if [[ $# != 1 ]]; then
                 echo "ERROR: Specify certificate argument"
                 exit 1
              fi
              CERT="$1"
              ${pkgs.openssl}/bin/openssl x509 -text -noout -in "$CERT"
            '');
          };
        });
}
