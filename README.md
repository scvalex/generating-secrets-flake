# generating-secrets-flake

This is a demo nix flake to generate TLS secrets with `cfssl`.  This
is not meant to be used as a library; copy what bits you like to your
own flake.

This code creates a CA, then generates certificates for three
Kubernetes nodes and three etcd peers.  If this looks like a lot of
certificates, that's because Kubernetes uses different certificates
for each component, and there are a lot of components.

See this blog post for more details: https://scvalex.net/posts/55/
